import './App.css';
import React from "react";
/* import { render } from "react-dom"; */

import Categories from'./components/Categories';
import HomePage from './components/HomePage';
import About from './components/About';
import RecipesList from './components/RecipesList';
import RecipesListAdmin from './components/Admin/RecipesListAdmin';
import AboutAdmin from './components/Admin/AboutAdmin'
import EditAboutus from'./components/Admin/EditAboutus'
import {
  BrowserRouter as Router,
  Route,
  Routes
} from "react-router-dom"
import Admin from './components/Admin/Admin';
import EditRecipes from './components/Admin/EditRecipes';
import Loading from './components/Loading';

function App() {
  return (<Router>
  <Routes>
<Route exact path="/" element={<HomePage/>}/>
<Route exact path="/categories" element={<Categories/>}/>
<Route exact path='/Loading' element={<Loading/>}/>
<Route exact path="/about" element={<About/>}/>
<Route exact path='/categories1' element={<RecipesList/>} />
<Route exact path='/admin' element={<Admin/>} />
<Route exact path='/admin/recipes' element={<RecipesListAdmin/>} />
<Route exact path='/admin/editrecipes' element={<EditRecipes/>} />
<Route exact path='/admin/aboutus' element={<AboutAdmin/>} />
<Route exact path='/admin/editaboutus' element={<EditAboutus/>} />
  </Routes>
  </Router>

  );
}


export default App;