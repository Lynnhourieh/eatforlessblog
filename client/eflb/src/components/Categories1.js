import React from "react";
import "./categories1.css";
import Share from "./Share";

function Categories1({ id, title, cost, procedure, time, image, ingredients }) {
  const filterIngredients = ingredients.filter((obj) => obj.Recipe._id === id);
  console.log(ingredients)
  return (
    <div className="container1">
      <div className="card-wrapper">
        <div className="card1 light">
          <div className="text-overlay">
            <div className="imagecat">
              <img src={`/ImagesTest/${image}`} alt="recipeImage" />
            </div>
            <h2 className="h2cat1">{title}</h2>
            <h2 className="h2cat1">Price:{cost}$</h2>
            <h2 className="h2cat1">Time:{time}minutes</h2>
          </div>
          <div className="purchase-button-container">
            <h2 className="back-h2 h2cat1">
              <ul>
{filterIngredients.map((item) => (<li>{item.Ingredient.name}</li>))}
              </ul>
            </h2>
            <i className="fa-solid fa-person"></i>
            <div className="purchase-button light">{procedure}</div>
          </div>
        </div>
        <div>
          <Share />
        </div>
      </div>
    </div>
  );
}

export default Categories1;
