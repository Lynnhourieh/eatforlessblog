
import React from 'react';
import Search from './Search';
import Categories from './Categories';
import Header from "./Header";
import Footer from "./Footer";



//HomePage component is for the whole page.
const HomePage = () => {
    return (
        <div>
            <Header />
            <Search />
            <Categories />
            <Footer />
        </div>

    )
}

export default HomePage
