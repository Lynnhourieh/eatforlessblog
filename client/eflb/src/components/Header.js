import React from "react";


import {Link} from 'react-router-dom'
import "./Header.css"
import Logo from "../image/Logo.png"
export default function Header() {
    return (
        <div className="containernav">
        <nav className="navbar">
        <img className="logo" src={Logo} alt="logo"></img>
        <div className="home headerPages">
         <Link to='/'>HOME</Link>
    
        </div>
        <div className="catnav headerPages">
         <Link to='/categories1'>Recipes</Link>
        </div>
        <div className="aboutnav headerPages">
          <Link to='/about'>ABOUT</Link>
        </div>
        </nav>
        </div>
    )
}

