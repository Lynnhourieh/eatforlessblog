import React from "react";
import './Search.css'
import Banner from "../image/Banner.jpg"
import cover from "../image/cover.jpeg"

const Search = () => {
    return (
        <div className="forBody">
            <nav className="all">

                <span>
                    <div className="content">
                        <h2>Quick Budget-<br></br>Friendly Recipes</h2>
                        <h2>Quick Budget-<br></br>Friendly Recipes</h2>
                    </div>
                    <div className="flexbox">
                        <br></br>
                        <div>
                            <p class="text">Enjoy delicious food without spending
                                too much time and money!</p>

                        </div>
                    </div>
                </span>
            </nav>
            <section className="section">
                <article className="article">
                    <img src={cover} className="banner test" alt="cover" />
                    <img src={Banner} className="banner test" alt="banner"/>

                </article>
            </section>
        </div>

    )
}

export default Search