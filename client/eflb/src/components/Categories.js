import React from "react";
import "./categories.css";
import {Link} from 'react-router-dom'
import {useState,useEffect} from "react";
import Loading from "./Loading";


function Categories() {
  //fetching data
  const [data, setData]=useState([])
  const [loading, setloading]=useState(true)
  useEffect(()=>{
    fetch("http://localhost:5000/categories", {
           method:'GET',
            headers: {
                 "Content-Type": "application/json"
            }
 })
    .then(reponse=>{
      if(reponse.ok){
        return reponse.json();
      } throw reponse;
    }).then(data=>{
      setloading(false)
      setData(data)
    })
  },[])
  if(loading) return <Loading />
  console.log(data.message[1].title);
  // console.log(data[0].title);
  return (
    <div>
  
    <div className="bodyCat">
      <div className="containerCat">
        <div className="title">
        <h3 contenteditable="true" className="h1cat1">Categories</h3>
          
        </div>
        
        <div className="categories">
          <div className="card">
            <div className="face face1">
              <div className="content">
                <img src="https://cdn-icons-png.flaticon.com/512/1584/1584808.png" alt="categoryTimeLogo"/>

                <Link to='/categories1'>{data.message[1].title}</Link>
              </div>
            </div>
          </div>
          <div className="card">
            <div className="face face1">
              <div className="content">
              <img src="https://www.pngkey.com/png/full/104-1044757_purse-vector-free-flat-design-wallet-icon-png.png"  alt="categoryBudgetLogo"/>
                <Link to='/categories1'>{data.message[0].title}</Link>
  
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    </div>
  );
}

export default Categories;
