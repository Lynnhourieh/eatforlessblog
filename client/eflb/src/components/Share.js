import React, { useState } from "react";
import './Share.css'









const Share = () => {
  const [show, setShow] = useState(false)
  const toggleOptions = () => {
    setShow(!show);
    console.log("Clicked")
  };
  return (
    <div className="wrapperShare">
      <button className="btnShare" onClick={toggleOptions}>
        <i className="ri-share-line"></i>
      </button>
      <ul className={`listShare ${show ? "active" : ""}`}>
        <li className="itemShare">
          <a target="blank" href="https://instagram.com" className="linkShare ig">
            <i className="ri-instagram-line"></i>
          </a>
        </li>
        <li className="itemShare" styles="--d: .25s;">
          <a target="blank" href="https://twitter.com" className="linkShare tw">
            <i className="ri-twitter-line"></i>
          </a>
        </li>
        <li className="itemShare" styles="--d: .5s;">
          <a target="blank" href="https://whatsapp.com" className="linkShare wa">
            <i className="ri-whatsapp-line"></i>
          </a>
        </li>
      </ul>
    </div>
  )
}

export default Share;