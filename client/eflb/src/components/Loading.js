import React from "react";
import "./Loading.css";

export default function Loading() {
    return(
        <div className="bodyLoad">
        <div className="loader">
        <div className="face">
          <div className="circle"></div>
        </div>
        <div className="face">
          <div className="circle"></div>
        </div>
      </div>
      </div>
    )
}
