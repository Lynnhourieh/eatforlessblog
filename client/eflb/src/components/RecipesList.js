import React from "react";
import { useState, useEffect } from "react";
import Header from "./Header";
import Footer from "./Footer";
import Categories1 from "./Categories1";
import Loading from "./Loading";

const RecipesList = () => {
  const [data, setData] = useState([]);
  const [data2, setData2] = useState([]);
  const [loading, setloading] = useState(true);
  const getRecipes = () => {
    fetch("http://localhost:5000/recipes", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((reponse) => {
        if (reponse.ok) {
          return reponse.json();
        }
        throw reponse;
      })
      .then((data) => {
        setloading(false);
        setData(data);
      });
  };

  const getRecipesIngrients = () => {
    fetch("http://localhost:5000/recipeingredient", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((reponse) => {
        if (reponse.ok) {
          return reponse.json();
        }
        throw reponse;
      })
      .then((data2) => {
        setloading(false);
        setData2(data2);
        console.log(data2);
      });
  };

  useEffect(() => {
    getRecipes();
    getRecipesIngrients();
  }, []);

  const onSubmitHandlers1 = async (e) => {
    e.preventDefault();
    fetch("http://localhost:5000/recipes", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((reponse) => {
        if (reponse.ok) {
          return reponse.json();
        }
        throw reponse;
      })
      .then((data) => {
        setloading(false);
        setData(data);
      });
  }; //for button less than 2$
  const onSubmitHandlers = async (e) => {
    e.preventDefault();
    await fetch(`http://localhost:5000/recipes/less`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((reponse) => {
        if (reponse.ok) {
          return reponse.json();
        }
        throw reponse;
      })
      .then((data) => {
        setData(data);
      });
  };

  //for button less than 10 minutes
  const onSubmitHandlerss = async (e) => {
    e.preventDefault();
    await fetch(`http://localhost:5000/recipes/min`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((reponse) => {
        if (reponse.ok) {
          return reponse.json();
        }
        throw reponse;
      })
      .then((data) => {
        setData(data);
      });
  };
  //working with searching by name of recipe
  const onSubmitHandler = async (e, name) => {
    console.log(name);
    e.preventDefault();

    await fetch(`http://localhost:5000/recipes/search/${name}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((reponse) => {
        if (reponse.ok) {
          return reponse.json();
        }
        throw reponse;
      })
      .then((data) => {
        setData(data);
      });
  };
  if (loading) return <Loading />;
  return (
    <div>
      <Header />

      <span className="choices">
        <div>
          <form
            className="search"
            onChange={(e) => onSubmitHandler(e, e.target.value)}
          >
            <input
              type="search"
              placeholder="What would you like today?"
            ></input>{" "}
          </form>
        </div>
        <form onSubmit={onSubmitHandlers1}>
          <button className="glow-on-hover">All</button>
        </form>
        <form onSubmit={onSubmitHandlers}>
          <button className="glow-on-hover">less than 2$</button>
        </form>
        <form onSubmit={onSubmitHandlerss}>
          <button className="glow-on-hover">Less than 10 minutes</button>
        </form>
      </span>
      <h3 className="h1cat1" contenteditable="true">
        Recipes List
      </h3>
      <div className="cards-align">
        {data.success === true
          ? data.val.map((data) => (
              <div className="mapcat">
                <Categories1
                  id={data._id}
                  ingredients={data2}
                  title={data.title}
                  cost={data.cost}
                  
                  image={data.image}
                  
                  like={data.like}
                  time={data.time}
                  procedure={data.procedure}
                />
              </div>
            ))
          : ""}
      </div>

      <Footer />
    </div>
  );
};

export default RecipesList;
