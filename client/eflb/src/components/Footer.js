import React from "react";

import "./Footer.css"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faFacebook,
    faTwitter,
    faInstagram,
    faYoutube

} from "@fortawesome/free-brands-svg-icons";
export default function Footer(){
    return (
        <div className="footer">
            <p>EAT FOR LESS | ALL RIGHTS RESERVED</p>
            <div className="icons">
                <div className="facebook">
                    <a target="blank" href="https://facebook.com"><FontAwesomeIcon icon={faFacebook} size="2x" /></a>
                </div>
                <div className="instagram">
                    <a target="blank" href="https://instagram.com"><FontAwesomeIcon icon={faInstagram} size="2x" /></a>
                </div>
                <div className="twitter">
                    <a target="blank" href="https://twitter.com"><FontAwesomeIcon icon={faTwitter} size="2x" /></a>
                </div>
                <div className="twitter">
                    <a target="blank" href="https://youtube.com"><FontAwesomeIcon icon={faYoutube} size="2x" /></a>
                </div>
            </div>
        </div>
    )
}
