import React from 'react';
import "./EditRecipes.css";
import { useLocation, useNavigate } from 'react-router-dom';
import { useState, useEffect } from "react";
import axios from "axios";
import HeaderAdmin from './HeaderAdmin';
export default function EditRecipes() {
    const location = useLocation();
    const [token,setToken]=useState(null)
    const [data,setData]=useState([]);
    const [id, setId] = useState(null);
    const [title, setTitle] = useState([])
    const [time, setTime] = useState([])
    const [cost, setCost] = useState([])
    const [like] = useState([])
    const [procedure, setProcedure] = useState([])
    const [image, setImage] = useState([])
    const [Category, setCategory] = useState([])
    const navigate=useNavigate();
    const onChangeFile = e => {

        setImage(e.target.files[0]);

    }
   
   
    const tokenRemove = ()=>{
      localStorage.removeItem('token');
      setToken(localStorage.getItem('token'))
      if(!token){
        navigate('/admin')
      }
    }
    useEffect(()=>{
      setToken(localStorage.getItem('token'))
      setInterval(()=>{
        tokenRemove()
      },43200000)
    })
    useEffect(()=>{
      const token= localStorage.getItem('token')
      if(!token){
        navigate('/admin')
      }
    },[])
    useEffect(() => {
      location.state.id ? setId(location.state.id) : setId(null);
  },[])
    const onSubmitHandler = () => {

        const formData = new FormData();
        formData.append("RecipeImage", image)
        formData.append("title", title)
        formData.append("cost", cost)
        formData.append("like", like)
        formData.append("time", time)
        formData.append("procedure", procedure)
        formData.append("Category", Category)
    
        axios
          .post("http://localhost:5000/recipes", formData, {
            method: 'POST',
            headers: { 'Content-Type': 'multipart/form-data' },
    
          })
          .then((res) => console.log('Successfuly Sent!'))
          .catch((err) => {
            console.log(err);
          });
      }
      const onSubmitHandlers = () => {

        const formData = new FormData();
        formData.append("RecipeImage", image)
        formData.append("title", title)
        formData.append("cost", cost)
        formData.append("like", like)
        formData.append("time", time)
        formData.append("procedure", procedure)
        formData.append("Category", Category)
        axios
          .put(`http://localhost:5000/recipes/${location.state.id}`, formData, {
    
            headers: { 'Content-Type': 'multipart/form-data' },
    
          })
          .then((res) => console.log('Successfuly Sent!'))
          .catch((err) => {
            console.log(err);
          });
      }
    
      const fetchRecipes = async () => {
        let str = (id)? "http://localhost:5000/recipes":`http://localhost:5000/recipes/${id||location.state.id}`

        await fetch(str, {
          method: 'GET',
          headers: {
            "Content-Type": "application/json"
          }
        })
          .then(reponse => {
            if (reponse.ok) {
              return reponse.json();
            } throw reponse;
          }).then(data => {
            setData(data)
            setTitle(data.title)
            setTime(data.time)
            setCost(data.cost)
            setCategory(data.Category)
            setProcedure(data.procedure)
          })
      }
    
    
      useEffect(() => {
        fetchRecipes()
        console.log(data)
      }, [])
    if (id === null) {
        return (
          <div>
            <HeaderAdmin/>
            <div class="form">
                <div class="title">ADD Recipes</div>
                <div class="subtitle">Add  your recipe here!</div>
                <div class="input-container ic1">
                    <input id="firstname" class="input" type="text" placeholder=" " value={title} onChange={(e) => { setTitle(e.target.value) }} required/>
                    <div class="cut"></div>
                    <label for="Title" class="placeholder">Title</label>
                </div>
                <div class="input-container ic2">
                    <input id="lastname" class="input" type="number" min="1" max="1000" placeholder=" " value={time} onChange={(e) => { setTime(e.target.value) }} required />
                    <div class="cut"></div>
                    <label for="lastname" class="placeholder">Time</label>
                </div>
                <div class="input-container ic2">
                    <input id="lastname" class="input" type="number" min="1" max="1000" placeholder=" " value={cost} onChange={(e) => { setCost(e.target.value) }} required/>
                    <div class="cut"></div>
                    <label for="lastname" class="placeholder">Cost</label>
                </div>
                <div class="input-container ic2">
                    <input id="lastname" class="input" type="text" placeholder=" " value={procedure} onChange={(e) => { setProcedure(e.target.value) }} required/>
                    <div class="cut"></div>
                    <label for="lastname" class="placeholder">Procedure</label>
                </div>

                <div class="input-container ic2">
                    <input id="lastname" class="input" type="text" placeholder=" " value={Category} onChange={(e) => { setCategory(e.target.value) }} required/>
                    <div class="cut"></div>
                    <label for="lastname" class="placeholder">Category</label>
                </div>
                <div class="input-container ic2">
                    <input id="lastname" class="input" type="file" placeholder=" " onChange={onChangeFile}/>
                    <div class="cut"></div>
                    <label for="lastname" class="placeholder">Upload image</label>
                </div>
                <button type="text" class="submit" onClick={onSubmitHandler}>ADD</button>
            </div>
            </div>)
    } else {
        return (
          <div>
            <HeaderAdmin/>
            <div class="form">
                <input type="text" value={id} />
                <div class="title">Edit Recipes</div>
                <div class="subtitle">Edit  your recipe here!</div>
                <div class="input-container ic1">
                    <input id="firstname" class="input" type="text" placeholder=" " value={title} onChange={(e) => { setTitle(e.target.value) }} required/>
                    <div class="cut"></div>
                    <label for="Title" class="placeholder">Title</label>
                </div>
                <div class="input-container ic2">
                    <input id="lastname" class="input" type="number" min="1" max="1000" placeholder=" " value={time} onChange={(e) => { setTime(e.target.value) }} required />
                    <div class="cut"></div>
                    <label for="lastname" class="placeholder">Time</label>
                </div>
                <div class="input-container ic2">
                    <input id="lastname" class="input" type="number" min="1" max="1000" placeholder=" "  value={cost} onChange={(e) => { setCost(e.target.value) }} required />
                    <div class="cut"></div>
                    <label for="lastname" class="placeholder">Cost</label>
                </div>
                <div class="input-container ic2">
                    <input id="lastname" class="input" type="text" placeholder=" "  value={procedure} onChange={(e) => { setProcedure(e.target.value) }} required/>
                    <div class="cut"></div>
                    <label for="lastname" class="placeholder">Procedure</label>
                </div>

                <div class="input-container ic2">
                    <input id="lastname" class="input" type="file" placeholder=" "  onChange={onChangeFile} />
                    <div class="cut"></div>
                    <label for="lastname" class="placeholder">Upload image</label>
                </div>
                <button type="text" class="submit" onClick={onSubmitHandlers}>EDIT</button>
            </div>
            </div>)
    }
}
