import React from 'react';
import "./EditRecipes.css";
import { useNavigate } from 'react-router-dom';
import { useState, useEffect } from "react";
import HeaderAdmin from './HeaderAdmin';
import axios from "axios";
export default function EditAboutus() {
  const [token,setToken]=useState(null);
  const navigate = useNavigate();
  const [data, setData] = useState([]);
  const [id] = useState(null);
  const [name, setname] = useState([]);
  const [image, setImage] = useState([]);
  const [description, setdescription] = useState([]);
  const onChangeFile = e => {

    setImage(e.target.files[0]);

  }

  // const onSubmitHandler = () => {

  //   const formData = new FormData();
  //   formData.append("RecipeImage", image)
  //   formData.append("name", name)
  //   formData.append("description", description)

  //   axios
  //     .post("http://localhost:5000/aboutus", formData, {
  //       method: 'POST',
  //       headers: { 'Content-Type': 'multipart/form-data' },

  //     })
  //     .then((res) => console.log('Successfuly Sent!'))
  //     .catch((err) => {
  //       console.log(err);
  //     });
  // }
  const onSubmitHandlers = () => {

    const formData = new FormData();
    formData.append("RecipeImage", image)
    formData.append("name", name)
    formData.append("description", description)

    axios
      .put(`http://localhost:5000/aboutus/62bd558296bc2fd7a5b30cea`, formData, {

        headers: { 'Content-Type': 'multipart/form-data' },

      })
      .then((res) => console.log('Successfuly Sent!'))
      .catch((err) => {
        console.log(err);
      });
  }

  const fetchAboutus = async () => {
    await fetch(`http://localhost:5000/aboutus?id=62bd558296bc2fd7a5b30cea`, {
      method: 'GET',
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(reponse => {
        if (reponse.ok) {
          return reponse.json();
        } throw reponse;
      }).then(data => {
        setData(data);
        setname(data.name);
        setImage(data.image)
        setdescription(data.description);

      })
  }
  const tokenRemove = ()=>{
    localStorage.removeItem('token');
    setToken(localStorage.getItem('token'))
    if(!token){
      navigate('/admin')
    }
  }
  useEffect(()=>{
    setToken(localStorage.getItem('token'))
    setInterval(()=>{
      tokenRemove()
    },43200000)
  })
  useEffect(()=>{
    const token= localStorage.getItem('token')
    if(!token){
      navigate('/admin')
    }
  },[])

  useEffect(() => {
    fetchAboutus()
    console.log(data)
  }, [])
  if (id === null) {
    return (
      <div>
         <HeaderAdmin/>
      <div class="form">
        <div class="title">Edit Aboutus</div>
        <div class="subtitle">Edit  your info here!</div>
        <div class="input-container ic1">
          <input id="firstname" class="input" type="text" placeholder=" " value={name} onChange={(e) => { setname(e.target.value) }} required />
          <div class="cut"></div>
          <label for="Title" class="placeholder">name</label>
        </div>
        <div class="input-container ic2">
          <input id="lastname" class="input" type="text" min="1" max="1000" placeholder=" " value={description} onChange={(e) => { setdescription(e.target.value) }} required />
          <div class="cut"></div>
          <label for="lastname" class="placeholder">description</label>
        </div>

        <div class="input-container ic2">
          <input id="lastname" class="input" type="file" placeholder=" " onChange={onChangeFile} />
          <div class="cut"></div>
          <label for="lastname" class="placeholder">Upload image</label>
        </div>
        <button type="text" class="submit" onClick={onSubmitHandlers}>EDIT</button>
      </div>
      </div>
    )} else {
      return (
        <div>
           <HeaderAdmin/>
        <div class="form">
          <div class="title">Edit Aboutus</div>
          <div class="subtitle">Edit  your info here!</div>
          <div class="input-container ic1">
            <input id="firstname" class="input" type="text" placeholder=" " value={name} onChange={(e) => { setname(e.target.value) }} required />
            <div class="cut"></div>
            <label for="Title" class="placeholder">name</label>
          </div>
          <div class="input-container ic2">
            <input id="lastname" class="input" type="number" min="1" max="1000" placeholder=" " value={description} onChange={(e) => { setdescription(e.target.value) }} required />
            <div class="cut"></div>
            <label for="lastname" class="placeholder">description</label>
          </div>

          <div class="input-container ic2">
            <input id="lastname" class="input" type="file" placeholder=" " onChange={onChangeFile} />
            <div class="cut"></div>
            <label for="lastname" class="placeholder">Upload image</label>
          </div>
          <button type="text" class="submit" onClick={onSubmitHandlers}>EDIT</button>
        </div>
        </div>)
    }
  }
