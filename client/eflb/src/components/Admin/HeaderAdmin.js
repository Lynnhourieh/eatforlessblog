import React from "react";

import {useNavigate } from 'react-router-dom';
import {Link} from 'react-router-dom'
import "../Header.css"
import Logo from "../../image/Logo.png"
export default function HeaderAdmin() {
  const navigate=useNavigate();
  const logOut=()=>{
    localStorage.removeItem('token')
    navigate('/admin')
  }
    return (
        <div className="containernav">
        <nav className="navbar">
        <img className="logo" src={Logo} alt="logo"></img>
        <div className="home headerPages">
    
        </div>
        <div className="catnav headerPages">
         <Link to='/admin/recipes'>Recipes</Link>
        </div>
        <div className="aboutnav headerPages">
          <Link to='/admin/aboutus'>AboutUs</Link>
        </div>
        <div className="sign headerPages">
         <button onClick={logOut}>LogOut</button>
        </div>
        </nav>
        </div>
    )
}

