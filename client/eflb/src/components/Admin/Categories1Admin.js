import React from "react";
import "../categories1.css";
import { useState, useEffect } from "react";
import {useNavigate} from "react-router-dom"
function Categories1Admin({title,cost,procedure,time,image,id}) {
  console.log(id)
  const [token,setToken]=useState(null);
  
  const navigate = useNavigate();

  const [setData1] = useState([])
  const toComponentB = () => {
    navigate('/admin/editrecipes', { state: { id: id } });
  }
  const onSubmitHandlerdelete = async (e) => {

    console.log(id)
    await fetch(`http://localhost:5000/recipes/${id}`, {
      method: 'DELETE',
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(reponse => {
        window.location.reload();
        if (reponse.ok) {
          return reponse.json();
        } throw reponse;
      }).then(data1 => {

        setData1(data1)
      })

  }
  const tokenRemove = ()=>{
    localStorage.removeItem('token');
    setToken(localStorage.getItem('token'))
    if(!token){
      navigate('/admin')
    }
  }
  useEffect(()=>{
    setToken(localStorage.getItem('token'))
    setInterval(()=>{
      tokenRemove()
    },43200000)
  })
  useEffect(()=>{
    const token= localStorage.getItem('token')
    if(!token){
      navigate('/admin')
    }
  },[])


  return (

    

      
      <div className="container1">
        <div className="card-wrapper">
          <div className="card1 light">
            <div className="text-overlay">
              <div className="imagecat"><img src={`/ImagesTest/${image}`} alt="recipeImage"/></div>
              <h2 className="h2cat1">{title}</h2>
              <h2 className="h2cat1">Price:{cost}$ </h2>
              <h2 className="h2cat1">Time:{time}minutes</h2>

             
            </div>
            <div className="purchase-button-container">
              <h2 className="back-h2 h2cat1">{title}</h2>
              <i className="fa-solid fa-person"></i>
              <div className="purchase-button light">{procedure}</div>
              <h2 className="h2cat1">
                <button className="editbutton" onClick={() => { toComponentB()}}>Edit</button>
                <button onClick={onSubmitHandlerdelete} className="editbutton">Delete</button></h2>
            </div>
          </div>
        </div>
      </div>

    
  );
}

export default Categories1Admin;