import { useNavigate } from 'react-router-dom';
import React from 'react';
import "./Admin.css";
import { useState, useEffect } from "react";
export default function Admin() {
    const [ data, setData]=useState([])
    const [username, setUser] = useState("")
    const [password, setpassword] = useState("")
    const navigate = useNavigate();
   

const onSubmitHandlerlogin = (e) => {
    e.preventDefault()
    const message = {username, password}

    fetch("http://localhost:5000/admin",{
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
         },
         body:JSON.stringify(message)

     },[])
        .then(response => {
             if (response.ok) {
                return response.json();
            } throw response;
        }).then(data =>{ 
            setData(data.message)
            if(data.admin)
       {
        localStorage.setItem('token',data.admin);
        navigate("/admin/recipes");
    }else{
      return alert("wrong username or password")
    }
 }, [])
}


useEffect(()=>{
    const token = localStorage.getItem('token');
    if(token){
        navigate('/admin/recipes')
    }
})

return (
    <div>
        <span className='bodyAdmin'>

            <div className="containerlogin">
                <section id="contentlogin">
                    <form action="">
                        <h1>Login Form</h1>
                        <div>
                            <input type="text" placeholder="username" required="" id="username" value={username} onChange={(e) => {
                                setUser(e.target.value)
                            }}
                            ></input>
                        </div>
                        <div>
                            <input type="password" placeholder="Password" required="" id="password" value={password} onChange={(e) => {
                                setpassword(e.target.value)
                            }} />
                        </div>
                        <div>
                            <input type="submit" value="Log in" onClick={onSubmitHandlerlogin}/>

                        </div>
                    </form>

                </section>
            </div>

        </span>
    </div>

)




}