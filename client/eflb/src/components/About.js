import React from "react";
import "./About.css";
import {useState,useEffect} from "react";
import Header from "./Header";
import Footer from "./Footer";
import Loading from "./Loading";


function About() {
  const [data, setData]=useState([])
  console.log(data)
  const [loading, setloading]=useState(true)
  useEffect(()=>{
    fetch("http://localhost:5000/aboutus", {
           method:'GET',
            headers: {
                 "Content-Type": "application/json"
            }
 })
    .then(reponse=>{
      if(reponse.ok){
        return reponse.json();
      } throw reponse;
    }).then(data=>{
      setloading(false)
      setData(data)
    })
  },[])
  if(loading) return <Loading />
  return (
    <div className="body">
      <Header/>
    <section className="section">
      <div className="title">
      <h3 contenteditable="true" className="h1cat1">About</h3>
        <p>
        Access cheap and tasty recipes
        </p>
      </div>
      <div className="about-center section-center">
        <article className="about-img">
          <img src={`/ImagesTest/${data.image}`} alt="" />
        </article>
        <article className="about">
            <div className="content" id="vision">
              <h4>{data.name}</h4>
              <div className="btn"> <span className="noselect"><p>{data.description}</p></span> </div>
          </div>
        </article>
      </div>
    </section>
    <Footer/>
    </div>
  );
}

export default About;
