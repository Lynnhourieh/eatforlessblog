const router = require('express').Router();
const { models } = require('mongoose');
const multer=require("multer");
let Recipe = require('../models/recipe.model');
let categories = require('../models/categories.model')
//First request
//find method is about getting data from mango
//find method return a promise

const storage = multer.diskStorage({
    destination: (req, file, callback)=>{
    callback(null,"../client/eflb/public/ImagesTest/");
    },
    filename: (req,file,callback)=> {
    callback(null, file.originalname);
    }
}) 
const upload = multer({storage: storage});
//rouing for filter for recipes that are less than 2 $
router.route('/less').get((req, res) => {
    Recipe.find({ Category: categories.id, Category: "62bc820b8dbba052aee7a922" })
        //then will return data from database
        .then(recipes => res.send({success:true, val:recipes}))
        .catch(err => res.status(400)
            .json("error: " + err))
})

//rouing for filter for recipes that are less than 10 minutes.
router.route('/min').get((req, res) => {
    Recipe.find({ Category: categories.id, Category: "62bc820b8dbba052aee7a923" })
        //then will return data from database
        .then(recipes => res.send({success:true, val:recipes}))
        .catch(err => res.status(400)
            .json("error: " + err))
})
router.route('/').get((req, res) => {
    Recipe.find()
        //then will return data from database
        .then(recipes =>  res.send({success:true, val:recipes}))
        .catch(err => res.status(400)
            .json("error: " + err))
})
router.route('/').post(upload.single("RecipeImage"),(req, res) => {
    const title = req.body.title;
    const image = req.file.originalname;
    const cost = Number(req.body.cost);
    const like = Number(req.body.like);
    const time = Number(req.body.time);
    const Category = (req.body.Category);
    const procedure = (req.body.procedure)
    const NewRecipe = new Recipe({
        image,
        title,
        cost,
        like,
        time,
        Category,
        procedure


    })
    NewRecipe.save().then(() => res.json('Recipe Added')).catch(err => res.status(400).json('Error: ' + err))

})

router.route('/:id').delete((req, res) => {
    Recipe.findByIdAndDelete(req.params.id)
        .then(() => res.json('Recipe deleted'))
        .catch(err => res.status(400).json('Error: ' + err))
})

router.route('/:id').put(upload.single("RecipeImage"),(req, res) => {

    Recipe.findOneAndUpdate(({ _id: req.params.id }), {
        $set: {
            title: req.body.title,
            image: req.file.originalname,
            cost: req.body.cost,
            like: req.body.like,
            time: req.body.time,
            Category: req.body.Category,
            procedure: req.body.procedure,

        }
    }, { new: true }, (err, val) => {
        if (val != null) {
            Recipe.find((err, val) => {
                res.send(val)
            })
        } else {
            res.status(404).send({ status: 404, error: true, message: `the Recipe  does not exist` })
        }
    })
})

router.route('/:id').get((req, res) => {
    Recipe.findById(req.params.id)
        .then(recipes => res.json(recipes))
        .catch(err => res.status(400).json('Error: ' + err))
})

router.get('/search/:title1?', (req, res) => {
    let title1 = req.params.title1
    if(title1){
      title1 = req.params.title1
      Recipe.find({  title:{$regex:title1} }, (err, val) => {
        if (Object.keys(val).length>0) {
            res.send({success:true, val:val})
        }
        else {
            res.json({success: false, msg:"error"})
        }
    })

    }
    else {
       
        res.json({success: false, msg:"you have to provide a title"})
    }
})
module.exports = router;