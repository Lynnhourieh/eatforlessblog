const router = require('express').Router();
const { models } = require('mongoose');

let AboutUs = require('../models/aboutus.model');
const multer = require("multer");

const storage = multer.diskStorage({
    destination: (req, file, callback) => {
        callback(null, "../client/eflb/public/ImagesTest/");
    },
    filename: (req, file, callback) => {
    callback(null, file.originalname);
    }
})
const upload = multer({ storage: storage });


router.get('/',(req, res) => {
    AboutUs.findById("62bd558296bc2fd7a5b30cea")
        .then(aboutUs => res.json(aboutUs))
        .catch(err => res.status(400).json('Error: ' + err))
})


router.route('/:id').put(upload.single("RecipeImage"), (req, res) => {

    AboutUs.findOneAndUpdate(({ _id: req.params.id }), {
        $set: {
            
            image: req.file.originalname,
            name: req.body.name,
            description: req.body.description,

        }
    }, { new: true }, (err, val) => {
        if (val != null) {
            AboutUs.find((err, val) => {
                res.send(val)
            })
        } else {
            res.status(404).send({ status: 404, error: true, message: ` information  does not exist` })
        }
    })
})

module.exports = router