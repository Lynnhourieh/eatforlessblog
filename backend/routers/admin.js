const router = require('express').Router();
const { models } = require('mongoose');
let Admin = require('../models/admin.model');
const jwt = require('jsonwebtoken');




router.post('/',async (req,res)=>{
    try{
        const admin= await Admin.findOne({
            username:req.body.username,
            password: req.body.password
        })
        if(admin){
            const token = jwt.sign({
                username: admin.username,
            },'secret123',{expiresIn:'12h'}
            )
            return res.json({status:'ok',admin:token})
        }else{
            return res.json({status:'error',admin:false})
        }
    }catch(err){
        console.log(err.message)
    }
})

module.exports = router