const router = require('express').Router();
const { models } = require('mongoose');
let RecipeIngredient = require('../models/RecipeIngredient');
let recipe = require("../models/recipe.model")
let Ingredient = require("../models/Ingredient.model")
//First request
//find method is about getting data from mango
//find method return a promise  
router.route('/:id').get((req, res) => {
    const id = req.params.id;
    RecipeIngredient.find({ Recipe: id }).populate("Recipe", ["title"]).populate("Ingredient", ["name"])
        //then will return data from database
        .then(recipesingredients => res.json(recipesingredients))
        .catch(err => res.status(400)
            .json("error: " + err))
})

//First request
//find method is about getting data from mango
//find method return a promise  
router.route('/').get((req, res) => {
    RecipeIngredient.find().populate("Recipe", ["title"]).populate("Ingredient", ["name"])
        //then will return data from database
        .then(recipesingredients => res.json(recipesingredients))
        .catch(err => res.status(400)
            .json("error: " + err))
})

router.route('/').post((req, res) => {
    const Recipe = req.query.Recipe;
    const Ingredient = req.query.Ingredient;
    const quantity=req.query.quantity;
    const NewRecipeIngredient = new RecipeIngredient({
        Recipe,
        Ingredient,
        quantity,


    })
    NewRecipeIngredient.save().then(() => res.json('RecipeIngredient Added')).catch(err => res.status(400).json('Error: ' + err))

})
router.route('/:id').delete((req, res) => {
    RecipeIngredient.findByIdAndDelete(req.params.id)
        .then(() => res.json('Ingredient from recipe is deleted'))
        .catch(err => res.status(400).json('Error: ' + err))
})
module.exports = router;