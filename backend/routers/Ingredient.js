const router = require('express').Router();
const { models } = require('mongoose');
let Ingredient = require('../models/Ingredient.model');

//First request
//find method is about getting data from mango
//find method return a promise
router.route('/').get((req, res) => {
    Ingredient.find()
    //then will return data from database
        .then(ingredient => res.json(ingredient))
        .catch(err => res.status(400)
        .json("error: " + err))
})
//not working
router.route('/').post((req,res)=>{
    const name=req.query.name;
    const newIngredient=new Ingredient({name});
    newIngredient.save().then(()=>res.json('Ingredient Added'))
    .catch(err =>res.status(400).json('Error: '+ err));
})
router.route('/:id').get((req, res) => {
    Ingredient.findById(req.params.id)
        .then(ingredient => res.json(ingredient))
        .catch(err => res.status(400).json('Error: ' + err))
})

router.route('/:id').delete((req, res) => {
    Ingredient.findByIdAndDelete(req.params.id)
        .then(() => res.json('Ingredient deleted'))
        .catch(err => res.status(400).json('Error: ' + err))
})


module.exports=router;