const router = require('express').Router();
const { models } = require('mongoose');
let Categories = require('../models/categories.model');

//First request
//find method is about getting data from mango
//find method return a promise
router.route('/').get((req, res) => {
    Categories.find((err,data) =>{
        if(!err)
        res.send({status:200,message:data})
        else
        res.send(err.message)
    })
        
}) 
// router.route('/').post((req, res) => {
//     const title = req.query.title;
//     const desciption=req.query.desciption;
//     const NewCategory = new Categories({
//         title,
//         desciption


//     })
//     NewCategory.save().then(() => res.json('Category Added')).catch(err => res.status(400).json('Error: ' + err))

// })
 router.route('/:id').get((req, res) => {
    Categories.findById(req.params.id)
        .then(category => res.json(category))
        .catch(err => res.status(400).json('Error: ' + err))
})

// router.route('/:id').delete((req, res) => {
//     Categories.findByIdAndDelete(req.params.id)
//         .then(() => res.json('Category deleted'))
//         .catch(err => res.status(400).json('Error: ' + err))
// }) 


router.route('/:id').put( (req, res) => {

    Categories.findOneAndUpdate(({ _id: req.params.id }), {
        $set: {
            title: req.query.title,
            description: req.query.desciption,
           
        }
    }, { new: true }, (err, val) => {
        if (val != null) {
            Categories.find((err, val) => {
                res.send(val)
            })
        } else {
            res.status(404).send({ status: 404, error: true, message: `the Catgeory  does not exist` })
        }
    })
})

module.exports = router