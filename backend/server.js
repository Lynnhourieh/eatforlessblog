//import express
const express = require('express');
//cors for fetching data
const cors = require("cors");
const app = express()
const port = 5000;
app.use(cors());
app.use(express.json())
app.listen(port,()=>{
    console.log(`Server is running on port: ${port}`)
})
const { default: mongoose } = require('mongoose');

//import mangoose
const mangoose = require('mongoose');
//connect mangoose
mangoose.connect("mongodb+srv://lynn:0000@cluster0.nrquz.mongodb.net/?retryWrites=true&w=majority");
//check if database is connected or nnot
const db = mangoose.connection;
db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function () {
    console.log('connected')
})

//require the router files
//link the order with it's page


const RecipeRouter=require('./routers/recipes');
app.use("/recipes",RecipeRouter)

const IngredientRouter=require('./routers/Ingredient');
app.use("/ingredients",IngredientRouter)

const CategoryRouter=require('./routers/categories');
app.use("/categories",CategoryRouter)

const aboutUsRouter=require('./routers/aboutus');
app.use("/aboutus",aboutUsRouter)

const RecipeIngredientRouter=require('./routers/RecipeIngredient');
app.use("/recipeingredient",RecipeIngredientRouter)

const AdminRouter=require('./routers/admin');
app.use("/admin",AdminRouter)