const mongoose = require('mongoose');
//For Recipe/Ingredient
const RecipeIngredientSchema = new mongoose.Schema( {
    Recipe: { type: mongoose.SchemaTypes.ObjectId, ref: "Recipe" },
    Ingredient: { type: mongoose.SchemaTypes.ObjectId, ref: "Ingredient" },
    quantity: String,

})
const RecipeIngredient = mongoose.model("RecipeIngredient", RecipeIngredientSchema);

async function insertDummyRecipeIngredientData() {
    try {
        await RecipeIngredient.insertMany([{
            Recipe: "62c0797a75d0935fce927704",
            Ingredient: "62c565d3681002a022f5c1c6",
            quantity: "200g"

        }])
    } catch (error) {
        console.log(error)
    }
} 

//insertDummyRecipeIngredientData()
module.exports=RecipeIngredient