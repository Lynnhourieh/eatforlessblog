const mongoose = require('mongoose');

const AboutUsSchema = new mongoose.Schema({
    description: String,
    image: String,
    name: String,

})
const AboutUs = mongoose.model("AboutUs", AboutUsSchema);
async function insertDummyAboutUsData() {
    try {
        await AboutUs.insertMany([{
            description: "bla bla bla",
            image:"12345",
          
            name:"name of this item"

        }])
    } catch (error) {
        console.log(error)
    }
}
//insertDummyAboutUsData()


module.exports=AboutUs