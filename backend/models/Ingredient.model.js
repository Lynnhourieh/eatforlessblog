const mongoose = require('mongoose');

//For Ingredient
const IngredientSchema = new mongoose.Schema({
    name: String,

})

//translate the code and its representation from MongoDB to the Node
const Ingredient = mongoose.model("Ingredient", IngredientSchema);

async function insertDummyIngredientData() {
    try {
        await Ingredient.insertMany([{
            name: "potato"

        }, {
            name: "lettuce",
        },
       {
            name: "pasta",
        },{
            name: "bread",
        } ])
    } catch (error) {
        console.log(error)
    }
}
//insertDummyIngredientData()
module.exports=Ingredient

