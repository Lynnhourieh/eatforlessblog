const mongoose = require('mongoose');
//For Recipe
const RecipeSchema = new mongoose.Schema({
    title: String,
    image: String,
    like: Number,
    procedure: String,
    time: Number,
    cost: Number,
    Category: { type: mongoose.SchemaTypes.ObjectId, ref: "Category" }
})
//indexing the recipe to search

//translate the code and its representation from MongoDB to the Node
const Recipe = mongoose.model("Recipe", RecipeSchema);


//function to insert fake Recipe data in database
async function inserDummyRecipeData() {
    try {
        await Recipe.insertMany([{
            title: "Chicken Burger",
            image: "chicken.png",
            procedure: "1-In a mixing bowl, combine the mayonnaise, sun-dried tomatoes, lemon juice, garlic, and rosemary. Season with a pinch of salt and black pepper. Set the aioli aside Preheat a grill, grill pan, or cast-iron skillet.Combine the ground chicken with 1⁄2 teaspoon salt and 1⁄2 teaspoon black pepper and mix gently.Without overworking the meat, form into four patties until the chicken just comes together.When the grill or skillet is hot (if using a skillet, add a touch of oil), add the burgers.Cook on the first side for 5 to 6 minutes, until a nice crust develops.Flip and cook for another 3 to 4 minutes, until the burgers are firm but ever so slightly yielding to the touch and cooked through.Remove the burgers. While the grill or pan is hot, toast the buns.Layer the bottom buns with the arugula, top each with a burger, then slather the aioli over the top of each.Crown with the bun tops and serve",
            time: 10,
            cost: 11,
            Category: "62b8322578cdfb207d981a03"
        }, {
            title: "Veggie Scramble With Mushrooms",
            image: "mashroom.png",
            like: 2,
            Category: "62b8322578cdfb207d981a03",
            procedure: "1-In a mixing bowl, combine the mayonnaise, sun-dried tomatoes, lemon juice, garlic, and rosemary. Season with a pinch of salt and black pepper. Set the aioli aside Preheat a grill, grill pan, or cast-iron skillet.Combine the ground chicken with 1⁄2 teaspoon salt and 1⁄2 teaspoon black pepper and mix gently.Without overworking the meat, form into four patties until the chicken just comes together.When the grill or skillet is hot (if using a skillet, add a touch of oil), add the burgers.Cook on the first side for 5 to 6 minutes, until a nice crust develops.Flip and cook for another 3 to 4 minutes, until the burgers are firm but ever so slightly yielding to the touch and cooked through.Remove the burgers. While the grill or pan is hot, toast the buns.Layer the bottom buns with the arugula, top each with a burger, then slather the aioli over the top of each.Crown with the bun tops and serve",
            time: 5,
            cost: 15,
        },
        {
            title: "Savory Waffle With Egg",
            image: "eggs.png",
            like: 2,
            Category: "62b8322578cdfb207d981a03",
            procedure: "The ingredients may sound like a strange combination, but stick with us! We know what we're doing. The flavors are nicely balanced and the portion size is perfect, if we do say so ourselves. Plus, most importantly, the nutritional profile of this savory waffle is just what you want for the most important meal of the day.",
            time: 5,
            cost: 15,
        }])
    } catch (error) {
        console.log(error)
    }
}



//inserDummyRecipeData()
//export 
module.exports = Recipe 