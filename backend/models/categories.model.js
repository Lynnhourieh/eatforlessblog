const mongoose = require('mongoose');
//For Categories
const CategorySchema = new mongoose.Schema({
    title: String,
    description: String
})
//translate the code and its representation from MongoDB to the Node
const Category = mongoose.model("Category", CategorySchema);

//function to insert fake Category data in database
async function inserDummyCategoryData() {
    try {
        await Category.insertMany([{
            title: "Less than 2 $",
            description: "This category to prepare recipes with low budget (less than 2 $)"
        }, {
            title: "Less than 10 minutes",
            description: "This category to prepare recipes immediatly (less than 10 minutes)"
        }])
    } catch (error) {
        console.log(error)
    }
}
//inserDummyCategoryData()
module.exports=Category
